
$(document).ready(function(){
  $(".amenities-content").owlCarousel({
      items:1,
//      autoplay:true,
      loop: true,
      margin: 30,
      nav:true,
      smartSpeed:700,
      autoplayHoverPause:true,
      dots:false,
      navText: ['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
  });
});

    

$(document).ready(function(){
    
    $('#stats').waypoint(function(){
    
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
        
        this.destroy();//it is used when we want to show counter only once.
        
    },{
        offset: 'bottom-in'
    });
    
});



$(document).ready(function(){
  $(".articles").owlCarousel({
      items:3,
      autoplay:false,
      loop: true,
      margin:20,
      center: true,
      nav:true,
      smartSpeed:700,
      autoplayHoverPause:true,
      dots:false,
      navText: ['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
  });
});





$(document).ready(function(){
    $(".cards").isotope({});
    
    $("#isotope-filters").on("click","button",function(){
        let filterValue = $(this).attr("data-filter");
        console.log(filterValue);
        $(".cards").isotope({
           filter: filterValue 
        });
        
        $("#isotope-filters").find('.button-active').removeClass('button-active');
        $(this).addClass('button-active');
    });
});

   